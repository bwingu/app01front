var BWingu = function() {
	
	/* --- VAR --- */

	this.language;
	this.defaultLang = "en";
	this.routes = {};

	/* --- PUBLIC --- */
	
	/**
	 * Check if the user is connected by checking the cookie "rememberme". 
	 * If not, he is redirected to the index. 
	 */
	this.checkConnection = function() {
		if(document.cookie.indexOf("rememberme") == -1) {
			//TODO : add an ajax call to check if the user is really connected
			window.location = "./index.html";
		}
	}
	
	/**
	 * Return the language. Detect it if not defined.
	 * */
	this.lang = function(aLang) {
		!this.language && this.setLang();
		aLang && this.setLang(aLang);
		return this.language;
	}
	
	/**
	 * Retrieve texts from the properties. 
	 * */
	this.text = function(category, txtID) {
		var msg = properties.text[this.lang()][category][txtID];
		if(!msg) {
			console.log("Message not found : [lang:" + this.lang() + "][category:" + category + "][txtID:" + txtID + "]");
			msg = properties.text[this.lang()].errors.general;
		}
		return msg;
	}
	
	/**
	 * store(key) or store(key, val)
	 * get or set value in the local storage (no time limit).
	 * */
	this.store = function(key, val) {
		if(localStorage) {
			val && (localStorage[key] = val);
			return localStorage[key];
		}  else {
			this.yourBrowserSucks();
		}
		
	}
	
	/** Obvious function... */
	this.yourBrowserSucks = function() {
		alert("Get a real browser !")
	}

	/** Change all texts of the page. */
	this.loadTexts = function(varLang) {
		varLang && this.lang(varLang);
		this.fillTexts();
	}
	
	/** Display a error message.
	 *  Parameter "err" is the ID the error have in the properties.
	 *  */
	this.displayError = function(err) {
		var messageElt = $("#message");
		messageElt.find("span").html(this.text("errors", err));
		messageElt.fadeIn().delay(2012).fadeOut();
	}
	
	/** XOR your string "value" with the "random" number. */
	this.myXOR = function(value, random) {
		var res = "";
		for(var i=0; i < value.length; ++i){
			res += String.fromCharCode(random^value.charCodeAt(i));
		}
		return res;
	} 
	
	
	/* --- PRIVATE --- */

	this.setUrls = function() {
		var _this = this;
		$.each(properties.urls, function(key, val) {
			_this.routes[key] = Route({ url: val });
		});
	}
	
	this.fillTexts = function() {
		var _this = this;
		$("[bwingu-text-category][bwingu-text-ID]").each(function() {
			var elt = $(this);
			var cat = elt.attr("bwingu-text-category");
			var id = elt.attr("bwingu-text-ID");
			var attr = elt.attr("bwingu-text-attr");
			
			if(id.indexOf("js:") != -1) {
				id = eval(id.substring(3));
			}
			
			var txt = _this.text(cat, id);
			
			if(attr) {
				elt.attr(attr, txt);
			} else {
				elt.html(txt);
			}
		});
	}
	
	this.init = function() {
		var _this = this;
		
		
		$(function() {
			//Requiered
			if($("#message").length == 0) {
				console.log("No html element for error messages .");
				return;
			}
			
			if(!properties) {
				console.log("No propertities object declared");
				return;
			}
			
			_this.setUrls();
			
			//Lang
			_this.loadTexts(_this.lang());
			
			$("[bwingu-switchLang]").click(function(e) {
				var lang = $(this).attr("bwingu-switchLang");
				if(lang != _this.lang()) {
					_this.loadTexts(lang);
				}
				e.preventDefault();
			});
			
			//Menu
			$(".btn-navbar").toggle(
				function() {
					var _this = $(this);
					var target = _this.attr("data-target");
					_this.siblings(target).slideDown("slow");
				}, 
				function() {
					var _this = $(this);
					var target = _this.attr("data-target");
					_this.siblings(target).slideUp("slow");
				}
			);
			
		});
	}
	
	this.setLang = function(aLang) {
		this.language = this.defaultLang;
		if(aLang) {
			this.language = aLang;
		} else if(this.store("lang")) {
			this.language = this.store("lang");
		} else if(navigator.language) {
			this.language = navigator.language.substring(0, 2);
			if(!properties.text[this.language]) {
				this.language = this.defaultLang;
			} 
		}
		this.store("lang", this.language);
	}
	
	/*OBJECT*/

	function Route(def) {
	    return function(options) {
	        // Set default request settings
	    	var settings = $.extend({
	        	url: def.url + (options.urlEnd || ""),
	        	type: options.type || "POST",
	            dataType: "json",
	            crossDomain: true,
	            data: null,
	            error: options.error || function() {
	            	if(!options.errorMessage) {
	            		console.log("'errorMessage' properties not defined in your Route call");
	            		return;
	            	}
	            	B.displayError(options.errorMessage);
	            }
	    	}, options);

	    	if(options.success) {
	    		settings.success = function(data) {
	    			if(data && data.codeRetour == 503) {
	    				document.cookie = "rememberme=; path=/";
	    				window.location = "./index.html";
	    			} else if(data && data.codeRetour) {
	    				B.displayError(data.codeRetour);
	    			} else {
	    				options.success(data);
	    			}
	    		}
	    	}
	    	
	    	if(!settings.statusCode) {
	    		settings.statusCode = {};
	    	}

	    	settings.statusCode = {
               	404: settings.statusCode[404] || function() {
           			alert("Page not found");
               	},
               	500: settings.statusCode[500] || function() {
               		alert("Error 500");
               	}
	    	}
	    	
	        // Send request
	        $.ajax(settings);

	    };
	}
	
	
	/* --- INIT --- */
	
	this.init();
}


var B = new BWingu();
