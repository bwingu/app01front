var aList;

$(function(){

	var connectAsGuest = function() {
		B.routes.login({
			data: {
				username: "guest",
				password: "guest"
			},
			success: function(data) {
				window.location = "./list.html?uuid=" + uuid;
			},
			errorMessage : "login"
		});
	}
	
	var letsLogin = function(login, psw, random) {
		B.routes.login({
			data: {
				username: login,
				password: $.base64.encode(B.myXOR(psw, random))
			},
			success: function(data) {
				if(uuid) {
					window.location = "./list.html?uuid=" + uuid;
				} else if(data.info) {
					B.store("userHasInfo", true);
					window.location = "./dashboard.html";
				} else {
					window.location = "./list.html";
				}
			},
			errorMessage : "login"
		});
	}
	
	
	// ****** BIND UI ******
	
	
	$("a.close").click(function(e) {
		$(this).parent().hide();
		e.preventDefault();
	});
	
	$("#login, #psw, #submit, #visitorBtn").ajaxSend(function() {
		$(this).attr("disabled", "disabled");
	});
	
	$("#login, #psw, #submit, #visitorBtn").ajaxComplete(function() {
		$(this).removeAttr("disabled");
	});
	
	$("#message").ajaxSend(function() {
		$(this).hide();
	});
	
	$("#visitorBtn").click(function(){
		window.location = "./list.html";
	});
	
	$("#loginForm").submit(function(e) {
		var _this = $(this);
		e.preventDefault();
		
		B.routes.random({
			success: function(data) {
				letsLogin(
					_this.find("#login").val(),
					_this.find("#psw").val(),
					data
				);
			},
			errorMessage : "getRandom"
		});
		
	});
	
	
	// ****** INIT ******
	
	var param = window.location.href.split("?");
	var uuid;
	if(param[1]) {
		var arg1 = param[1].split("=");
		if(arg1[1] && arg1[0] == "uuid") {
			uuid = arg1[1];
		}
	}
	var isConected = document.cookie.indexOf("rememberme") != -1;
	
	if(isConected) {
		if(uuid) {
			window.location = "./list.html?uuid=" + uuid;
		} else {
			window.location = "./board.html";
		}
	} else {
		if(uuid) {
			$("#visitorBtn").show().click(connectAsGuest);
		} else {
			$("#visitorBtn").hide();
		} 
	}
	
});