
var DashList = function(selector, request) {
	
	this.dest = {};
	
	this.displayData = function(data) {
		var _this = this;
		_this.empty();
		$.each(data.lists, function(key, val) {
			_this.append(
				$("<li>").append(
					$("<a>").html(val.name).attr("href", "./list.html?" + val.uuid)
				)
			);
		});
	}
	
	this.init = function(selectr, reqst) {
		var _this = this;
		$(function() {
			_this.dest = $(selectr);
			_this.dest.html("<li>Loading...</li>");
			B.routes[reqst]({
				success:_this.displayData,
				errorMessage: "dashboardLists"
			});
		});
	}
	
	this.init(selector, request);
}

new DashList("#ownersLists", "ownersLists");
new DashList("#visitedLists", "visitedLists");