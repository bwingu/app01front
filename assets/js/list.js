var aList;

$(function(){

	var getList = function(uuid) {
		
		$("h1").html(B.text("misc", "load"));
		
		B.routes.showList({
			urlEnd: uuid + ".json",
			type: "GET",
			success: function(data) {
				aList = data;
				$("#createList").submit(addItem);
				fillMail(aList.uuid);
				fillList(aList);
			},
			errorMessage : "listRetrieve"
		});
		
	}
	
	var fillMail = function(uuid) {
		$("#send").attr("href", "mailto:?subject=" + B.text("misc", "mailSubject") + "&body= "+ B.text("misc", "mailBody") + properties.urls.showList + uuid);
	}
	
	var fillList = function(list) {
		$("h1").html(list.text);
		var listNameInput = $("#listName");
		listNameInput.val("");
		listNameInput.attr("placeholder", B.text("misc", "itemPlaceholder"));
		var ul = $("#items").hide();
		$.each(list.elements, function(){
			var li = $("<li>").html(this.text);
			ul.prepend(li);
		});
		ul.slideDown("slow");
	}
	
	var addList = function(e) {
		e.preventDefault();
		var listNameInput = $("#listName");
		if(listNameInput.val() == "") {
			B.displayError("empty");
			return;
		}
		var _this = $(this);
		
		B.routes.addlist({
			data: {
				text: listNameInput.val()
			},
			success: function(data) {
				aList = data;
				fillMail(aList.uuid);
				$("h1").html(listNameInput.val());
				listNameInput.val("");
				listNameInput.attr("placeholder", B.text("misc", "itemPlaceholder"));
				_this.unbind().submit(addItem);
			},
			errorMessage : "listSave"
		});
		
	}
		
	var addItem = function(e) {
		e.preventDefault();
		var listNameInput = $("#listName");
		if(listNameInput.val() == "") {
			B.displayError("empty");
			return;
		}
		
		B.routes.addItem({
			urlEnd: aList.uuid + "/" + listNameInput.val(),
			data: {
				text: $(this).find("#listName").val()
			},
			success: function(data) {
				var li = $("<li>").html(listNameInput.val()).hide();
				$("#items").prepend(li);
				li.slideDown("slow");
				listNameInput.val("");
			},
			errorMessage : "itemSave"
		});
		
	}
	
	
	
	// ****** BIND UI ******

	
	
	$("#homeBtn").click(function(e) {
		if(!confirm(B.text("misc", "leave"))) {
			e.preventDefault();
		}
	});
	
	$("a.close").click(function(e) {
		$(this).parent().hide();
		e.preventDefault();
	});
	
	$("#send").click(function(e) {
		if(aList && aList.uuid) {
			//window.location = $(this).attr("href");
		} else {
			e.preventDefault();
		}
	});
	
	$("#listName, #submit").ajaxSend(function() {
		$(this).attr("disabled", "disabled");
	});
	
	$("#listName, #submit").ajaxComplete(function() {
		$(this).removeAttr("disabled");
	});
	
	$("#message").ajaxSend(function() {
		$(this).hide();
	});
	
	
	
	// ****** INIT ******
	
	//Is there a uuid in the url ?
	var param = window.location.href.split("?");
	if(param[1]) {
		var uuid = param[1].split("=");
		if(uuid[1]) {
			getList(uuid[1]);
		} else {
			$("#createList").submit(addList);
		}
	} else {
		$("#createList").submit(addList);
	}
	
});