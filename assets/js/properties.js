var properties = {
	
	urls: {
		showList:		"http://www.bwingu.com/list/",
		addlist:		"http://www.bwingu.com/list.json",
		addItem:		"http://www.bwingu.com/element/",
		login:    		"http://www.bwingu.com/ElementActions/login",
		random:   		"http://www.bwingu.com/SecurityPlugin/secureRandom",
		gotLists:		"http://www.bwingu.com/list/readListCount ",
		ownersLists:	"http://www.bwingu.com/list/readListOwner",
		visitedLists:	"http://www.bwingu.com/list/readListParticipants" 
	},
	text: {
		en: {
			errors: {
				general: 		"Oops, something went wrong.",
				listSave:		"Oops, can't save the list. Try again.",
				listRetrieve: 	"Oops, can't retrieve the list. Relaod.",
				itemSave: 		"Oops, can't save the item. Try again.",
				empty: 			"Please type something.",
				login: 			"Error while login in.",
				getRandom: 		"Fail to communicate with Get It.",
				dashboardLists:	"Oops, can't retrive the lists.",
				201: 			"Invalid password.",
				203: 			"Invalid login or password.",
				500: 			"Oops, something went wrong.",
				503: 			"Your're not connected."
			},
			misc: {
				load: 				"Loading...",
				home: 				"Home",
				mailSubject: 		"Someone share a list with you",
				mailBody: 			"Throw your idea here : ",
				listPlaceholder: 	"Got a list name ?",
				itemPlaceholder: 	"Throw your ideas",
				leave: 				"Are you sure ? \n You will loose the list if you have not send it. Leave the liste anyway ?"
			}
		},
		fr: {
			errors: {
				general: 		"Oups, une erreur s'est produite.",
				listSave: 		"Oups, impossible d'enregistrer la liste. Ré-essayez.",
				listRetrieve: 	"Oups, impossible de récupérer la liste. Rechargez la page.",
				itemSave: 		"Oups, impossible d'enregistrer l'item. Ré-essayez.",
				empty: 			"Le champ est vide.",
				login: 			"Erreur lors de la connexion.",
				getRandom: 		"Impossible de communiquer avec Get It.",
				dashboardLists:	"Oups, impossible de récupérer les lists.",
				201: 			"Mot de passe incorecte.",
				203: 			"Login ou mot de passe incorecte.",
				500: 			"Oups, une erreur s'est produite.",
				503: 			"Vous n'êtes pas connecté."
			},
			misc: {
				load: 				"Chargement...",
				home: 				"Accueil",
				mailSubject: 		"Quelqu'un souhaite partager une liste avec vous.",
				mailBody: 			"Partagez vous aussi vos idées ici : ",
				listPlaceholder: 	"Vous avez un nom de liste ?",
				itemPlaceholder: 	"Partagez vos idées",
				leave: 				"Vous allez quitter cette liste. \n Vous la perdrez si vous ne l'avez pas envoyé . Quitter la page ?"
			}
		}
	}
	
}